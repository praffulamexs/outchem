(function () {
  'use strict';

  angular
    .module('app')
    .factory('InterceptorFactory', InterceptorFactory);

  InterceptorFactory.$inject = ['$rootScope', 'UserService'];

  function InterceptorFactory($rootScope, UserService) {
    return {
      request: prepareRequestUrl,
      responseError: handleResponseError
    };

    function prepareRequestUrl(config) {
      if (config.url.indexOf("identity/connect") >= 0 ||
        config.url.indexOf("api/v1/userservice") >= 0) {
        return config;
      }

      var currentUser = UserService.getCurrentUser();
      var accessToken = currentUser ? currentUser.accessToken : null;
      if (accessToken) {
        config.headers.authorization = 'Bearer ' + accessToken;
      }
      return config;
    }

    function handleResponseError(response) {
      if (response.status === 401) {
        $rootScope.$broadcast('unauthorized');
      }
      return response;
    }
  }
})();
