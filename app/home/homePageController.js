(function () {
  'use strict';

  angular
  .module('app')
  .controller('HomePageController', HomePageController);

  HomePageController.$inject = ['$scope', '$state', '$uibModal', 'dataService', '$timeout', 'bsLoadingOverlayService', 'CATEGORY', 'Utils'];


  function HomePageController($scope, $state, $uibModal, dataService, $timeout, bsLoadingOverlayService, categoryConst, Utils) {
    var vm = this;
    vm.showContactUs = true;
    vm.contact = {
      contactId: '00000000-0000-0000-0000-000000000000',
      contactName: '',
      contactEmail: '',
      contactComments: '',
      mobileNo: 0
    };
    vm.stats = {};
    vm.servSlides = [];
    vm.prodSlides = [];
    vm.imgSlides = [{
        image: '../assets/images/homepageslider/homeimage1-new.png',
        id: 1
        },
        {
            image: '../assets/images/homepageslider/homepage2-new.png',
            id: 2
        },
		{
            image: '../assets/images/homepageslider/homepage3-new.png',
            id: 3
        },
		{
            image: '../assets/images/homepageslider/homepage4-new.png',
            id: 4
        }];
    var currentIndex = 0;

    vm.getCount = function () {
      dataService.getCount().then(function (resp) {
        vm.stats = resp.data;
      }, function (err) {
        console.log('error', err);
      });
    };

    vm.getService = function () {
      dataService.getCategoryByID(categoryConst.service).then(function (resp) {
        vm.serviceData = angular.copy(resp.data);
        vm.servSlides = createSlides(resp.data.splice(0, 5));
      }, function (err) {
        console.log('error', err);
      });
    };

    vm.getProduct = function () {
      dataService.getCategoryByID(categoryConst.product).then(function (resp) {
        vm.productData = angular.copy(resp.data);
        vm.prodSlides = createSlides(resp.data.splice(0, 5));
      }, function (err) {
        console.log('error', err);
      });
    };

    function createSlides(data) {
      var slides = [];
      currentIndex = 0;
      angular.forEach(data, function (_obj) {
        slides.push({
          name: _obj.keywordName,
          description: _obj.keywordDescription,
          id: currentIndex++
        });
      });
      return slides;
    }

    function activate() {
      bsLoadingOverlayService.start();
      bsLoadingOverlayService.stop();
      vm.submit = submitData;
      vm.getService();
      vm.getProduct();
      vm.getCount();
      recordLocation();
    }

    function recordLocation() {
      if(!Utils.isLocationRecorded){
        dataService.getLocationExternal().then(function (response) {
          if (response.status === 200) {
            dataService.setLocation(response.data).then(function (resp) {
              if (resp.status === 200) {
                Utils.setLocationRecorded();
              }
            })
          }
        })
      }
    }

    vm.getKeywords = function (key) {
      return dataService.getKeywordBySearchKey(key).then(function (resp) {
        console.log(resp);
        return resp.data.keywords.map(function (item) {
          return item;
        });
      }, function (err) {
        console.log('error', err);
        return [];
      });
    };

    function submitData() {
      bsLoadingOverlayService.start();
    }

    vm.btnGoSearchClk = function (_srchTxt) {
      $state.go('search', {searchTxt: _srchTxt});
    };

    vm.onSelect = function ($item) {
        vm.btnGoSearchClk($item.keywordId);
    };

    vm.showAllKeywords = function (_type) {
      var _keywords = [];
      if (_type === 'Product') {
        _keywords = vm.productData;
      } else {
        _keywords = vm.serviceData;
      }
      $uibModal.open({
        animation: true,
        templateUrl: '/app/misc/keywordsModal.html',
        resolve: {
          data: function () {
            return {
              type: _type,
              keywords: _keywords
            };
          }
        },
        controller: function ($scope, data) {
          $scope.keywords = data.keywords;
          $scope.type = data.type;
        }
      });
    };

    activate();

    vm.validateContact = function () {
      var mobileRgx = /^[0]?[789]\d{9}$/;
      var emailRgx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if (angular.isUndefined(vm.fName) || vm.fName === null || vm.fName === '') {
        vm.fNameError = true;
      } else {
        vm.fNameError = false;
      }
      if (angular.isUndefined(vm.mobile) || vm.mobile === null || vm.mobile === '' || !(mobileRgx.test(vm.mobile))) {
        vm.mobileError = true;
      } else {
        vm.mobileError = false;
      }
      if (angular.isUndefined(vm.email) || vm.email === null || vm.email === '' || !(emailRgx.test(vm.email))) {
        vm.emailError = true;
      } else {
        vm.emailError = false;
      }
      if (!vm.fNameError && !vm.mobileError && !vm.emailError) {
        vm.contact.contactName = vm.fName;
        vm.contact.contactEmail = vm.email;
        vm.contact.mobileNo = vm.mobile;
        vm.contact.contactComments = vm.comment;
        dataService.saveContactUs(vm.contact).then(function (result) {
          vm.contactSuccess = true;
          vm.fName = '';
          vm.email = '';
          vm.mobile = '';
          vm.comment = '';
          $timeout(function () {
            vm.contactSuccess = false;
            vm.showContactUs = false;
          }, 5000);
          console.log(result);
        }, function (err) {
          console.log(err);
        });
      }
    };
  }
})();
