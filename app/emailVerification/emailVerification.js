﻿(function () {
  'use strict';
  angular
    .module('app')
    .controller('emailVerificationController', emailVerificationController);

  emailVerificationController.$inject = ['$rootScope', '$state', '$scope', 'dataService', '$timeout', '$location'];

  function emailVerificationController($rootScope, $state, $scope, dataService, $timeout, $location) {
    var vm = this;
    vm.shwWarning = false;
    vm.shwExpError = false;
    vm.shwSuccess = false;
    vm.shwError = false;

    console.log('code', $location.search().code);
    console.log('userID', $location.search().userId);

    dataService.verifyEmail($location.search().userId, $location.search().code).then(function (resp) {
      if (resp) {
        if (resp.status === 200) {
          vm.shwSuccess = true;
          vm.shwWarning = false;
          $timeout(function () {
            $state.go('app');
          }, 5000);
        } else {
          vm.shwError = true;
          vm.shwWarning = false;
        }
      }
      //  if (resp.data.expired) {
      //  vm.shwExpError = true;
      //  vm.shwWarning = false;
      //  vm.newVerifyLink = '';
      //  }
    }, function (err) {
      vm.shwError = true;
      vm.shwWarning = false;
      console.log('Error In getContractor', err);
    });
  }
})();
