(function () {
  'use strict';

  angular
    .module('app')
    .service('dataService', dataService);

  dataService.$inject = ['$http', 'toastr', 'bsLoadingOverlayService'];

  function dataService($http, toastr, bsLoadingOverlayService) {
    var BASE_URL = "https://services.outchem.com/";
    // var BASE_URL = "http://localhost:8010/";
    var BASE_URL_API = "api/v1/";
    var registrationUrl = BASE_URL + BASE_URL_API + "userservice";
    // Login And Authorization
    //var identityUrl = "https://localhost:44365/identity/connect/";
    var identityUrl = "https://identity.outchem.com/identity/connect/";
    var tokenUrl = identityUrl + "token";

    // Reveal Module Pattern is used
    return {
      getImageUrl: getImageUrl,
      doLogin: doLogin,
      getUserInfo: getUserInfo,
      getKeyword: getKeyword,
      getKeywordsByCategoryName: getKeywordsByCategoryName,
      getKeywordById: getKeywordById,
      getKeywordBySearchKey: getKeywordBySearchKey,
      getKeywordCategoryWise: getKeywordCategoryWise,
      saveKeyword: saveKeyword,
      logKeywordSearch: logKeywordSearch,
      findContractor: findContractor,
      findBuyer: findBuyer,
      getPackage: getPackage,
      getPackageById: getPackageById,
      savePackage: savePackage,
      getPackageListing: getPackageListing,
      getPackageListingById: getPackageListingById,
      savePackageListing: savePackageListing,
      getCategories: getCategories,
      getCategoryByID: getCategoryByID,
      saveCategories: saveCategories,
      saveBuyers: saveBuyers,
      saveBuyerInformation: saveBuyerInformation,
      getBuyerInformation: getBuyerInformation,
      saveContactUs: saveContactUs,
      saveContractors: saveContractors,
      getCount: getCount,
      register: register,
      addCategoryKeywords: addCategoryKeywords,
      addContractorPackage: addContractorPackage,
      getContractor: getContractor,
      getContractorProfile: getContractorProfile,
      getContractorProgress: getContractorProgress,
      verifyEmail: verifyEmail,
      getFacilities: getFacilities,
      verifyPhone: verifyPhone,
      verifyPhoneCode: verifyPhoneCode,
      forgetPassword: forgetPassword,
      resetPassword: resetPassword,
      getUser: getUser,
      getLoggedInUser: getLoggedInUser,
      imageUpload: imageUpload,
      getImage: getImage,
      updateCategories : updateCategories,
      deleteCategories : deleteCategories,
      getAllKeywords: getAllKeywords,
      getKeywordDetails : getKeywordDetails,
      updateKeyword : updateKeyword,
      deleteKeyword : deleteKeyword,
      getPricing : getPricing,
      updatePricing : updatePricing,
      getTrendingKeywordsData : trendingKeywordsData,
      getContractorRatings : getContractorRatings,
      saveContractorRatings : saveContractorRatings,
      updatePackage : saveEditPackage,
      deletePackage : deletePackage,
      contactMeInfo: contactMeInfo,
      logContractorListing: logContractorListing,
      getContractorPackage: getContractorPackage,
      getKeywordsUsageData : getKeywordsUsageData,
      getCompaniesContacted : getCompaniesContacted,
      getKeywordsPrice : getKeywordsPrice,
      getPackageSummary : getPackageSummary,
      getCompaniesContactedByRatings : getCompaniesContactedByRatings,
      getLocationExternal : getLocationExternal,
      setLocation : setLocation,
      getLocation : getLocation,
      saveFacility : saveFacility,
      editFacility : editFacility,
      deleteFacility: deleteFacility,
      getAffiliations : getAffiliations,
      editAffiliation : editAffiliation,
      saveAffiliation : saveAffiliation,
      deleteAffiliation : deleteAffiliation,
      getUsersCountryWise : getUsersCountryWise,
      getCountrywiseWebsiteHits : getCountrywiseWebsiteHits,
      getRemindersList : getRemindersList,
      reviewReminder : reviewReminder,
      editUser: editUser
    };

    // INTERNAL FUNCTIONS

    function makeLoginData(dataToSend) {
      var data = {
        username: dataToSend.userId,
        password: dataToSend.userPassword,
        grant_type: "password",
        scope: "ocAPI openid profile email"
      };
      var body = "";
      for (var key in data) {
        if (body.length) {
          body += "&";
        }
        body += key + "=";
        body += encodeURIComponent(data[key]);
      }
      return body;
    }

    // END INTERNAL FUNCTIONS

    // API SERVICE EXPOSED FUNCTIONS

    function doLogin(dataToSend) {
      return $http({
        method: 'POST',
        url: tokenUrl,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Basic b3V0Y2hlbS53ZWJzaXRlOm91dGNoZW1zZWNyZXQ='
        },
        transformRequest: function () {
          return makeLoginData(dataToSend);
        },
        data: ''
      }).then(function (response) {
        if (response.status === 200 && response.data.access_token) {
          return response.data;
        }
      }, getAvengersFailed);
    }

    function getImageUrl() {
      return "https://s3.ap-south-1.amazonaws.com/outchem/images/";
    }

    function getUserInfo(accessToken) {
      return $http({
        method: 'GET',
        url: BASE_URL + BASE_URL_API + 'userservice',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Bearer ' + accessToken
        },
        data: ''
      }).then(function (response) {
        if (response.status === 200) {
          return response.data;
        }
      }, getAvengersFailed);
    }

    function register(registrationData) {
      return $http.post(registrationUrl, registrationData);
    }

    function getAvengersFailed(response) {
      toastr.error("Something Went Wrong. Check Console Log.", "Error");
      console.log(response);
      bsLoadingOverlayService.stop();
    }

    function getKeyword(unCategorized) {
      return $http.get(BASE_URL + BASE_URL_API + 'keyword', {
        params: {
          unCategorized: unCategorized ? unCategorized : false
        }
      });
    }

    function getKeywordsByCategoryName(cName) {
      return $http.get(BASE_URL + BASE_URL_API + 'keyword/GetKeywordsByCategoryName', {
        params: {
          cName: cName
        }
      });
    }

    function getKeywordById(kId) {
      return $http.get(BASE_URL + BASE_URL_API + 'keyword/GetKeywordById', {
        params: {
          kId: kId
        }
      });
    }

    function verifyEmail(userId, code) {
      return $http.post(BASE_URL + BASE_URL_API + 'userservice/ConfirmEmail?userId=' + userId +
        "&code=" + window.encodeURIComponent(code));
    }

    function getKeywordBySearchKey(searchKey, cId) {
      return $http.get(BASE_URL + BASE_URL_API + 'keyword', {
        params: {
          searchKey: searchKey,
          cId: cId ? cId : '00000000-0000-0000-0000-000000000000'
        }
      });
    }

    function getKeywordCategoryWise(searchKey, select, kId) {
      return $http.get(BASE_URL + BASE_URL_API + 'keyword/GetKeywordCategoryWise', {
        params: {
          searchKey: searchKey,
          select: select ? select : false,
          _kId: kId
        }
      });
    }

    function saveKeyword(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'keyword', data);
    }

    function getPackage() {
      return $http.get(BASE_URL + BASE_URL_API + 'package');
    }

    function getPackageById(pId) {
      return $http.get(BASE_URL + BASE_URL_API + 'package/GetPackageById', {
        params: {
          pId: pId
        }
      });
    }

    function savePackage(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'package', data);
    }

    function getPackageListing() {
      return $http.get(BASE_URL + BASE_URL_API + 'packageListing');
    }

    function getPackageListingById(UserID) {
      return $http.get(BASE_URL + BASE_URL_API + 'packageListing/GetListingPackageByUserID', {
        params: {
          UserID: UserID
        }
      });
    }

    function savePackageListing(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'packageListing', data);
    }

    function getCategories() {
      return $http.get(BASE_URL + BASE_URL_API + 'categories');
    }

    function getCategoryByID(id) {
      return $http.get(BASE_URL + BASE_URL_API + 'categories/' + id);
    }

    function saveCategories(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'categories', data);
    }

    function saveBuyers(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'buyers', data);
    }

    function saveBuyerInformation(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'buyerinformation', data);
    }

    function saveContactUs(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'contactus', data);
    }

    function getContractor() {
      return $http.get(BASE_URL + BASE_URL_API + 'contractors');
    }

    function getContractorProfile(cId) {
      return $http.get(BASE_URL + BASE_URL_API + 'contractors/GetContractorProfile', {
        params: {
          cId: cId
        }
      });
    }

    function saveContractors(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'contractors', data);
    }

    function addCategoryKeywords(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'contractors/AddContractorCategoryKeywords', data);
    }

    function addContractorPackage(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'contractors/addContractorPackage', data);
    }

    function getContractorProgress() {
      return $http.get(BASE_URL + BASE_URL_API + 'contractors/getContractorProgress');
    }

    function getCount() {
      return $http.get(BASE_URL + BASE_URL_API + 'count/GetCount');
    }

    function findContractor(searchKey, isPaid, location) {
      return $http.get(BASE_URL + BASE_URL_API + 'keyword/FindContractor', {
        params: {
          searchKey: searchKey,
          paid: isPaid || false,
          location: location || 0
        }
      });
    }

    function findBuyer(searchKey) {
      return $http.get(BASE_URL + BASE_URL_API + 'keyword/FindBuyer',{
        params:{
          searchKey : searchKey
        }
      });
    }

    function getBuyerInformation() {
      return $http.get(BASE_URL + BASE_URL_API + 'buyerinformation');
    }

    function getFacilities() {
      return $http.get(BASE_URL + BASE_URL_API + 'Facilities');
    }

    function verifyPhone(phone, accessToken) {
      // $http.defaults.headers.common.Authorization = 'Bearer ' + accessToken;
      return $http.get(BASE_URL + BASE_URL_API + 'userservice/VerifyPhone', {
        params: {phoneNumber: phone},
        headers: {Authorization: 'Bearer ' + accessToken}
      });
    }

    function verifyPhoneCode(phone, otp, accessToken) {
      return $http.get(BASE_URL + BASE_URL_API + 'userservice/VerifyPhoneCode', {
        params: {phoneNumber: phone, code: otp},
        headers: {Authorization: 'Bearer ' + accessToken}
      });
    }

    function getUser(contId) {
      return $http.get(BASE_URL + BASE_URL_API + 'userservice/GetUser', {
        params: {
          userId: contId
        }
      });
    }

    function getLoggedInUser(accessToken) {
      // $http.defaults.headers.common.Authorization = 'Bearer ' + accessToken;
      return $http.get(BASE_URL + BASE_URL_API + 'userservice/getLoggedInUser', {
        headers: {Authorization: 'Bearer ' + accessToken}
      });
    }

    function getImage(url, accessToken) {
      return $http.get(url, {
        headers: {Authorization: 'Bearer ' + accessToken}
      });
    }

    function forgetPassword(userID) {
      return $http.post(BASE_URL + BASE_URL_API + 'userservice/ForgotPassword?email=' + userID);
    }

	function editUser(objUser) {
      return $http.post(BASE_URL + BASE_URL_API + 'userservice/edit' , objUser, {
        headers: {Authorization: 'Bearer ' + objUser.accessToken}
      });
    }

    function resetPassword(userID, code, password) {
      return $http.post(BASE_URL + BASE_URL_API + 'userservice/ResetPassword?userId=' + userID +
        '&code=' + window.encodeURIComponent(code) + '&password=' + window.encodeURIComponent(password));
    }

    function logKeywordSearch(kId) {
      return $http.post(BASE_URL + BASE_URL_API + 'keyword/LogKeywordSearch?kId=' + kId);
    }

    function imageUpload(data) {
      var dataAsFormData = new FormData();
      dataAsFormData.append("attachment", data);
      return $http({
        url: BASE_URL + BASE_URL_API + 'imageservice',
        method: 'POST',
        data: dataAsFormData,
        transformRequest: angular.identity,
        headers: {'Content-Type': undefined}
      });
    }

    function deleteCategories(cid){
      return $http.post(BASE_URL + BASE_URL_API + 'categories' + '/Delete?cid='+cid);
    }

    function updateCategories(data){
      return $http.post(BASE_URL + BASE_URL_API + 'categories' + '/Edit',data);
    }

    function getAllKeywords(){
      return $http.get(BASE_URL + BASE_URL_API + 'keyword' + '/false');
    }

    function getKeywordDetails(top30){
      return $http.get(BASE_URL + BASE_URL_API + 'keyword' + '/GetKeywordAdmin',{
        params : {
          top30 : top30 || false
        }
      });
    }

    function updateKeyword (data) {
      return $http.post(BASE_URL + BASE_URL_API + 'keyword' + '/Edit', data);
    }

    function deleteKeyword (kid) {
      return $http.post(BASE_URL + BASE_URL_API + 'keyword' + '/Delete?kId=' + kid);
    }

    function getPricing () {
      return $http.get(BASE_URL + BASE_URL_API + 'PriceModel');
    }

    function updatePricing(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'PriceModel', data);
    }

    function trendingKeywordsData (format, fromDate) {
      return $http.get(BASE_URL + BASE_URL_API + 'Count/GetTrendingKeyword',{
        params: {
          graphType: format,
          graphDate: fromDate
        }
      });
    }

    function getContractorRatings (contId) {
      return $http.get(BASE_URL + BASE_URL_API + 'ContractorRating?cId=' + contId);
    }

    function saveContractorRatings (rating) {
      return $http.post(BASE_URL + BASE_URL_API + 'ContractorRating', rating);
    }

    function saveEditPackage (data) {
      return $http.post(BASE_URL + BASE_URL_API + 'package/edit', data);
    }

    function deletePackage (pId) {
      return $http.post(BASE_URL + BASE_URL_API + 'package/delete', pId);
    }

    function logKeywordSearch(kId) {
        return $http.post(BASE_URL + BASE_URL_API + 'keyword' + '/LogKeywordSearch?kid=' + kId);
    }

    function contactMeInfo(contactData) {
        return $http.post(BASE_URL + BASE_URL_API + 'ContactMeInfo', contactData);
    }

    function logContractorListing (data) {
      return $http.post(BASE_URL + BASE_URL_API + 'contractors/ContractorListingLog', data);
    }

    function getContractorPackage () {
      return $http.get(BASE_URL + BASE_URL_API + 'contractors/GetContractorPackage');
    }

    function getKeywordsUsageData (keywordId, startDate, endDate) {
      return $http.get(BASE_URL + BASE_URL_API + 'count/GetContractorListing',{
        params:{
          kId: keywordId,
          startDt : startDate,
          EndDt: endDate
        }
      });
    }

    function getCompaniesContacted () {
      return $http.get(BASE_URL + BASE_URL_API + 'count/TopContactedContractor');
    }

    function getKeywordsPrice (keywordId) {
      return $http.get(BASE_URL + BASE_URL_API + 'keyword/GetKeywordsPrice',{
        params:{
          kId: keywordId
        }
      })
    }

    function getPackageSummary () {
      return $http.get(BASE_URL + BASE_URL_API + 'count/PackageSummaryAllContractor');
    }

    function getCompaniesContactedByRatings () {
      return $http.get(BASE_URL + BASE_URL_API + 'contractorRating/GetTopContractorRating');
    }

    function getLocationExternal() {
      return $http.get('http://ip-api.com/json');
    }

    function setLocation(data) {
      return $http.post(BASE_URL + BASE_URL_API + 'Location',data);
    }

    function getLocation(cCode, rCode) {
      return $http.get(BASE_URL + BASE_URL_API + 'Location',{
        params: {
          countryCode : cCode || null,
          regionCode : rCode || null
        }
      })
    }

    function saveFacility (facilityToSave) {
      return $http.post(BASE_URL + BASE_URL_API + 'facilities', facilityToSave);
    }

    function editFacility (facilityToEdit) {
      return $http.post(BASE_URL + BASE_URL_API + 'facilities/edit', facilityToEdit);
    }

    function deleteFacility (fId) {
      return $http.post(BASE_URL + BASE_URL_API + 'facilities/delete?fId=' + fId);
    }

    function getAffiliations () {
      return $http.get(BASE_URL + BASE_URL_API + 'AffAssociation');
    }

    function editAffiliation (affilitationToEdit) {
      return $http.post(BASE_URL + BASE_URL_API + 'AffAssociation/edit', affilitationToEdit);
    }

    function saveAffiliation (affiliationToSave) {
      return $http.post(BASE_URL + BASE_URL_API + 'AffAssociation', affiliationToSave);
    }

    function deleteAffiliation (aId) {
      return $http.post(BASE_URL + BASE_URL_API + 'AffAssociation/delete?aId=' + aId);
    }

    function getUsersCountryWise(accessToken) {
      return $http.get(BASE_URL + BASE_URL_API + 'userservice/UserCountCountrywise',{
        headers: {Authorization: 'Bearer ' + accessToken}
      });
    }

    function getCountrywiseWebsiteHits () {
      return $http.get(BASE_URL + BASE_URL_API + 'count/CountrywiseWebsiteHits');
    }

    function getRemindersList() {
      return $http.get(BASE_URL + BASE_URL_API + 'count/PendingReviewList');
    }

    function reviewReminder(reminderToSend) {
      return $http.post(BASE_URL + BASE_URL_API + 'contractorRating/ReviewRemainder',reminderToSend);
    }
    // END API SERVICE EXPOSED FUNCTIONS
  }
})();
