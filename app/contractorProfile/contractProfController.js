﻿(function() {
    'use strict';
    angular
        .module('app')
        .controller('ContractProfController', ContractProfController);
    ContractProfController.$inject = ['$scope', 'dataService', 'bsLoadingOverlayService', '$timeout', '$filter', 'CATEGORY', 'UserService', 'toastr', 'Utils'];

    function ContractProfController($scope, dataService, bsLoadingOverlayService, $timeout, $filter, categoryConst, UserService, toastr, Utils) {
        var vm = this;
        vm.progress = 60;
        vm.contractor = {};
        vm.companyDetail = {};
        vm.objPackage = {};
        vm.selectedKeywords = [];
        vm.selectedProductKeywords = [];
        vm.companyDetail.Transportation = 'No';
        vm.companyDetail.PackagingServices = 'No';
        // vm.affiliatedAssociations = ['The Indian Pharmaceutical Association', 'Indian Drug Manufacturers&#39; Association', 'Organisation of Pharmaceutical Producers of India Peninsula Chambers', 'Bulk Drug Manufacturers Association'];
        vm.affiliatedAssociations = [];
        vm.locations = ['Mumbai', 'Pune', 'Nashik', 'Delhi', 'Chandigarh', 'Surat', 'Zansi'];
        // vm.Facilities = ['Schedule M Facility', 'Modular Facility', 'Germfree Facility'];
        vm.Facilities = [];
        vm.companyDetail.location = vm.locations[0];
        // vm.companyDetail.Facilities = ['Facility 2', 'Facility 3'];
        vm.countries = [{
            countryID: 1,
            name: "India"
        }];
        vm.companyDetail.arrRegulatoryBodies = [];

        activate();

        function activate() {
            vm.user = UserService.getCurrentUser();
            dataService.getLoggedInUser(vm.user.accessToken).then(function(resp) {
                if (resp.status === 200) {
                    Utils.isImage(dataService.getImageUrl() + resp.data + '.jpg').then(function(result) {
                        if (result) {
                            $scope.tempUrl = dataService.getImageUrl() + resp.data + '.jpg';
                        }
                    });
                }
                bsLoadingOverlayService.stop();
            }, function() {
                bsLoadingOverlayService.stop();
            });
            vm.user.countryName = $filter('filter')(vm.countries, { countryID: parseInt(vm.user.country, 16) });
            if (vm.user.countryName) {
                vm.user.countryName = vm.user.countryName[0].name;
            }
            bsLoadingOverlayService.start();
            dataService.getPackage().then(function(resp) {
                vm.packages = resp.data.packages;
                vm.objPackage.packageId = vm.packages[0].pId;
                bsLoadingOverlayService.stop();
            }, function(err) {
                bsLoadingOverlayService.stop();
                console.log('Error In getPackage', err);
                vm.packages = [];
            });
            dataService.getFacilities().then(function(resp) {
                // resp.data.facilities.forEach(function (element) {
                //   vm.Facilities.push(element.facilityName);
                // });
                var facilityNameArr = resp.data.facilities.map(function(el) {
                    return el.facilityName;
                });
                vm.Facilities = facilityNameArr;
                bsLoadingOverlayService.stop();
            }, function(err) {
                bsLoadingOverlayService.stop();
                console.log('Error In getFacility', err);
                // vm.packages = [];
            });
            dataService.getAffiliations().then(function (resp) {
              var affAssociationArr = resp.data.affAsso.map(function (el) {
                return el.affAssociationName;
              });
              vm.affiliatedAssociations = affAssociationArr;
            }, function(err) {
                bsLoadingOverlayService.stop();
                console.log('Error In getAffiliations', err);
            });
        }

        function bindTabData(_data) {
            vm.companyDetail.companyName = _data.companyName;
            vm.companyDetail.companyAddress = _data.companyAddress;
            vm.companyDetail.companyDesc = _data.companyDescription;
            vm.companyDetail.TechnicalSpecs = _data.technicalSpecs;
            vm.companyDetail.MinCapacity = _data.minCapacity;
            vm.companyDetail.MaxCapacity = _data.maxCapacity;
            vm.companyDetail.Transportation = _data.transportation ? _data.transportation : 'No';
            vm.companyDetail.PackagingServices = _data.packagingServices ? _data.packagingServices : 'No';

            if (_data.keywords) {
                vm.selectedKeywords = $filter('filter')(_data.keywords, { categoryId: categoryConst.service });
                vm.selectedProductKeywords = $filter('filter')(_data.keywords, { categoryId: categoryConst.product });
            }
            if (_data.regulatoryBodies) {
                vm.companyDetail.regulatoryBodies = _data.regulatoryBodies;
                vm.companyDetail.arrRegulatoryBodies = vm.companyDetail.regulatoryBodies.split(',');
                $scope.checkboxModel = {};
                vm.companyDetail.arrRegulatoryBodies.forEach(function(elem) {
                    $scope.checkboxModel[elem] = elem;
                });
            } else {
                vm.companyDetail.regulatoryBodies = '';
            }
            if (_data.affiliatedAssociations) {
                vm.companyDetail.affiliatedAssociations = _data.affiliatedAssociations.split(',');
            }
            if (_data.facilities) {
                vm.companyDetail.Facilities = _data.facilities.split(',');
            }
            if (_data.packages) {
                vm.addedPackages = _data.packages;
            }
        }

        vm.getTabData = function() {
            bsLoadingOverlayService.start();
            dataService.getContractor().then(function(resp) {
                bindTabData(resp.data);
                bsLoadingOverlayService.stop();
            }, function(err) {
                bsLoadingOverlayService.stop();
                console.log('Error In getContractor', err);
            });
        };
		
		vm.editProfile = function(field) {
			if(field === 'phone')
			{
				vm.edtPhone = false;
				bsLoadingOverlayService.start();
				dataService.editUser(vm.user).then(function (resp) {
					if (resp.status === 200) {
						toastr.success("Phone Updated Successful");
						vm.user.phoneNumberConfirmed = false;
					}
					bsLoadingOverlayService.stop();
				  }, function () {
					toastr.error("Error");
					bsLoadingOverlayService.stop();
				 });
			} else {
				vm.edtName = false;
				bsLoadingOverlayService.start();
				dataService.editUser(vm.user).then(function (resp) {
					if (resp.status === 200) {
						toastr.success("Name Updated Successful");					
					}
					bsLoadingOverlayService.stop();
				  }, function () {
					toastr.error("Error");
					bsLoadingOverlayService.stop();
				 });
			}		
		};

        vm.updateProgress = function() {
            bsLoadingOverlayService.start();
            dataService.getContractorProgress().then(function(response) {
                bsLoadingOverlayService.stop();
                vm.progress = response.data;
                // vm.progress = 42;
                if (vm.progress === 100) {
                    vm.enbPackageTab = false;
                } else {
                    vm.enbPackageTab = true;
                }
            }, function(err) {
                bsLoadingOverlayService.stop();
                console.log('Error In getContractorProgress', err);
            });
        };
        vm.getTabData();
        vm.updateProgress();
        vm.getKeywords = function(key) {
            return dataService.getKeywordBySearchKey(key, categoryConst.service).then(function(resp) {
                return resp.data.keywords.map(function(item) {
                    return item;
                });
            }, function(err) {
                console.log('Error', err);
                return [];
            });
        };

        vm.getProductKeywords = function(key) {
            return dataService.getKeywordBySearchKey(key, categoryConst.product).then(function(resp) {
                return resp.data.keywords.map(function(item) {
                    return item;
                });
            }, function(err) {
                console.log('Error', err);
                return [];
            });
        };

        vm.getPackages = function(key) {
            // return dataService.getPackage(key).then(function (resp) {
            return dataService.getKeywordBySearchKey(key).then(function(resp) {
                return resp.data.keywords.map(function(item) {
                    return item;
                });
            }, function(err) {
                console.log('Error', err);
                return [];
            });
        };

        vm.validateForm = function() {
            if (angular.isUndefined(vm.companyDetail.companyName) || vm.companyDetail.companyName === null || vm.companyDetail.companyName === '') {
                vm.errCompanyName = true;
            } else {
                vm.errCompanyName = false;
            }

            if (angular.isUndefined(vm.companyDetail.companyAddress) || vm.companyDetail.companyAddress === null || vm.companyDetail.companyAddress === '') {
                vm.errCompanyAddress = true;
            } else {
                vm.errCompanyAddress = false;
            }
            if (angular.isUndefined(vm.companyDetail.companyDesc) || vm.companyDetail.companyDesc === null || vm.companyDetail.companyDesc === '') {
                vm.errCompanyDesc = true;
            } else {
                vm.errCompanyDesc = false;
            }
            if (!vm.errCompanyName || !vm.errCompanyAddress || !vm.errCompanyDesc) {
                activate();
                vm.contractor.contractorId = "";
                vm.contractor.companyLogo = "";
                vm.contractor.companyName = vm.companyDetail.companyName;
                vm.contractor.companyAddress = vm.companyDetail.companyAddress;
                vm.contractor.companyDescription = vm.companyDetail.companyDesc;
                vm.contractor.businessPicture = "";
                // vm.contractor.review = "";
                // vm.contractor.rating = 0;
                // vm.contractor.regulatoryBodies = "";
                // vm.contractor.affiliatedAssociations = "";
                bsLoadingOverlayService.start();
                dataService.saveContractors(vm.contractor).then(function(result) {
                    vm.contactorSuccess = true;
                    bsLoadingOverlayService.stop();
                    $timeout(function() {
                        vm.contactorSuccess = false;
                    }, 5000);
                    vm.updateProgress();
                    console.log(result);
                }, function(err) {
                    bsLoadingOverlayService.stop();
                    console.log(err);
                });
                vm.updateProgress();
            }
        };

        vm.submitCategories = function() {
            vm.contractor = {};
            vm.contractor.keywords = vm.selectedKeywords;
            vm.contractor.keywords = vm.contractor.keywords.concat(vm.selectedProductKeywords);
            if (vm.companyDetail.arrRegulatoryBodies.length > 0) {
                vm.contractor.regulatoryBodies = vm.companyDetail.arrRegulatoryBodies.join(',');
            } else {
                vm.contractor.regulatoryBodies = '';
            }
            if (vm.companyDetail.affiliatedAssociations) {
                vm.contractor.affiliatedAssociations = vm.companyDetail.affiliatedAssociations.join(',');
            } else {
                vm.contractor.affiliatedAssociations = '';
            }
            if (vm.companyDetail.Facilities) {
                vm.contractor.Facilities = vm.companyDetail.Facilities.join(',');
            } else {
                vm.contractor.Facilities = '';
            }
            vm.contractor.TechnicalSpecs = vm.companyDetail.TechnicalSpecs;
            vm.contractor.Transportation = vm.companyDetail.Transportation;
            vm.contractor.PackagingServices = vm.companyDetail.PackagingServices;
            vm.contractor.MaxCapacity = vm.companyDetail.MaxCapacity;
            vm.contractor.MinCapacity = vm.companyDetail.MinCapacity;
            bsLoadingOverlayService.start();
            dataService.addCategoryKeywords(vm.contractor).then(function(result) {
                vm.contactorSuccess = true;
                bsLoadingOverlayService.stop();
                $timeout(function() {
                    vm.contactorSuccess = false;
                }, 5000);
                window.scrollTo(0, 0);
                vm.updateProgress();
                console.log(result);
            }, function(err) {
                bsLoadingOverlayService.stop();
                console.log(err);
            });
        };

        vm.addKeyword = function(item) {
            vm.selectedKeywords.push(item);
            vm.companyDetail.Keyword = '';
        };

        vm.removeKeyword = function(indx) {
            vm.selectedKeywords.splice(indx, 1);
        };

        vm.addProductKeyword = function(item) {
            vm.selectedProductKeywords.push(item);
            vm.companyDetail.productKey = '';
        };

        vm.removeProductKeyword = function(indx) {
            vm.selectedProductKeywords.splice(indx, 1);
        };

        vm.reguBodyChange = function(reguBody, imgName) {
            if (reguBody) {
                vm.companyDetail.arrRegulatoryBodies.push(imgName);
            } else {
                var index = vm.companyDetail.arrRegulatoryBodies.indexOf(imgName);
                if (index > -1) {
                    vm.companyDetail.arrRegulatoryBodies.splice(index, 1);
                }
            }
        };

        vm.changePackage = function(packageId) {
            vm.objPackage.savedTotalPrice = vm.objPackage.savedPrice * parseInt($filter('filter')(vm.packages, { pId: packageId })[0].days);
        }

        vm.addPackage = function(item) {
            vm.objPackage.keywordId = item.keywordId;
            getKeywordPrice(item.keywordId);
        };

        function getKeywordPrice(keywordId) {
            dataService.getKeywordsPrice(keywordId).then(function(response) {
                vm.objPackage.savedPrice = parseInt(response.data);
                vm.objPackage.savedTotalPrice = vm.objPackage.savedPrice * parseInt($filter('filter')(vm.packages, { pId: vm.objPackage.packageId })[0].days);
            })
        }

        vm.submitPackage = function() {
            vm.objPackage.contractorId = '';
            vm.objPackage.days = $filter('filter')(vm.packages, { pId: vm.objPackage.packageId })[0].days;
            vm.objPackage.location = vm.companyDetail.location;

            bsLoadingOverlayService.start();
            dataService.addContractorPackage(vm.objPackage).then(function(result) {
                vm.contactorSuccess = true;
                bsLoadingOverlayService.stop();
                $timeout(function() {
                    vm.contactorSuccess = false;
                }, 5000);
                vm.getTabData();
                vm.updateProgress();
                vm.packageEdit = false;
                vm.objPackage.packageId = vm.packages[0].pId;
                vm.companyDetail.location = vm.locations[0];
                vm.companyDetail.packages = '';
                vm.objPackage.contractorPackageId = 0;
                vm.objPackage.delete = false;
                console.log(result);
            }, function(err) {
                bsLoadingOverlayService.stop();
                console.log('contractor Package Error', err);
            });
        };

        vm.editPackage = function(pckg) {
            console.log(pckg);
            vm.packageEdit = true;
            vm.objPackage.packageId = pckg.packageId;
            vm.companyDetail.location = pckg.location;
            vm.objPackage.contractorPackageId = pckg.contractorPackageId;
            vm.companyDetail.packages = pckg.keywordName;
            if (pckg.keywordId) {
                getKeywordPrice(pckg.keywordId);
            }
        };

        vm.deletePackage = function(pckg) {
            console.log(pckg);
            vm.objPackage.contractorPackageId = pckg.contractorPackageId;
            vm.objPackage.delete = true;
            vm.submitPackage();
        };

        vm.cancelEdit = function() {
            vm.packageEdit = false;
            vm.objPackage.packageId = vm.packages[0].pId;
            vm.companyDetail.location = vm.locations[0];
            vm.companyDetail.packages = '';
        };

        vm.verifyMobile = function() {
            vm.showOTP = true;
            dataService.verifyPhone('091' + vm.user.phone, vm.user.accessToken).then(function(resp) {
                if (resp.status === 200) {
                    vm.verifyMobileClk = true;
                    toastr.success("OTP Sent Successful");
                } else {
                    toastr.error("Error In Sending OTP");
                }
            }, function(err) {
                toastr.error("Error In Sending OTP");
                console.log('validate OTP error', err);
            });
        };

        vm.validOTP = function() {
            vm.showOTP = false;
            dataService.verifyPhoneCode('091' + vm.user.phone, vm.OTP, vm.user.accessToken).then(function(resp) {
                if (resp.status === 200) {
                    vm.user.phoneNumberConfirmed = true;
                    toastr.success("OTP Validated Successful");
                } else {
                    toastr.error("OTP Validation error");
                }
            }, function(err) {
                toastr.error("OTP Validation error");
                console.log('OTP Validation error', err);
            });
        };

        vm.resendOTP = function() {
            console.log('resend');
        };

        $scope.$watch('model.profPic', function(newFile) {
            if (angular.isUndefined(newFile) === false) {
                $scope.tempUrl = URL.createObjectURL(newFile);
                dataService.imageUpload($scope.model.profPic).then(function(resp) {
                    console.log(resp);
                    if (resp.status === 201) {
                        toastr.success("Profile Pic Saved Successful");
                    } else {
                        toastr.error("Profile Pic Saving Failed");
                    }
                }, function(err) {
                    console.log('Image Upload error', err);
                    toastr.error("Profile Pic Saving Failed");
                });
            } else {
                $scope.tempUrl = '';
                // service call
            }
        });
    }

    angular
        .module('app')
        .directive('ngConfirmClick', ngConfirmClick);

    function ngConfirmClick() {
        return {
            link: function(scope, element, attr) {
                var msg = attr.ngConfirmClick;
                var clickAction = attr.confirmedClick;
                element.bind('click', function() {
                    if (window.confirm(msg)) {
                        scope.$eval(clickAction);
                    }
                });
            }
        };
    }

    angular
        .module('app')
        .directive('focusMe', focusMe);

    focusMe.$inject = ['$timeout', '$parse'];

    function focusMe($timeout, $parse) {
        return {
            // scope: true,   // optionally create a child scope
            link: function(scope, element, attrs) {
                var model = $parse(attrs.focusMe);
                scope.$watch(model, function(value) {
                    if (value === true) {
                        $timeout(function() {
                            element[0].focus();
                        });
                    }
                });
                // on blur event:
                element.bind('blur', function() {
                    scope.$apply(model.assign(scope, false));
                });
            }
        };
    }

    angular
        .module('app')
        .directive('fileModel', fileModel);

    fileModel.$inject = ['$parse'];

    function fileModel($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function() {
                    scope.$apply(function() {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }
})();
