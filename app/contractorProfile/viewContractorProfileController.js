﻿(function () {
  'use strict';

  angular
  .module('app')
  .controller('viewContractorProfileController', viewContractorProfileController);

  viewContractorProfileController.$inject = ['$scope', '$state', '$filter', 'bsLoadingOverlayService', 'dataService', 'CATEGORY', '$location', 'Utils', 'UserService', 'toastr'];

  function viewContractorProfileController($scope, $state, $filter, bsLoadingOverlayService, dataService, categoryConst, $location, Utils, UserService, toastr) {
    var vm = this;
    vm.const = categoryConst;
    vm.contractorID = $location.search().contractorId;
    vm.imageUrl = dataService.getImageUrl() + vm.contractorID + '.jpg';

    vm.getData = function () {
      bsLoadingOverlayService.start();
      dataService.getUser(vm.contractorID).then(function (rep) {
        if (rep.status === 200) {
          vm.user = rep.data;
        }
      });
      Utils.isImage(dataService.getImageUrl() + vm.contractorID + '.jpg').then(function (result) {
        if (result) {
          $scope.tempUrl = dataService.getImageUrl() + vm.contractorID + '.jpg';
        }
      });
      dataService.getContractorProfile(vm.contractorID).then(function (resp) {
        vm.contractorData = resp.data;
        vm.perRating = (vm.contractorData.avgRating / 5) * 100;
        if (vm.contractorData.keywords) {
          vm.services = $filter('filter')(vm.contractorData.keywords, {categoryId: categoryConst.service});
          vm.products = $filter('filter')(vm.contractorData.keywords, {categoryId: categoryConst.product});
        }

        if (vm.contractorData.regulatoryBodies) {
          vm.regulatoryBodies = vm.contractorData.regulatoryBodies;
          vm.arrRegulatoryBodies = vm.regulatoryBodies.split(',');
          $scope.checkboxModel = {};
          vm.arrRegulatoryBodies.forEach(function (elem) {
            $scope.checkboxModel[elem] = elem;
          });
        } else {
          vm.regulatoryBodies = '';
        }

        if (vm.contractorData.affiliatedAssociations) {
          vm.affiliatedAssociations = vm.contractorData.affiliatedAssociations.split(',');
        }

        if (vm.contractorData.facilities) {
          vm.facilities = vm.contractorData.facilities.split(',');
        }

        bsLoadingOverlayService.stop();
      }, function (err) {
        bsLoadingOverlayService.stop();
        console.log('Error In getContractor', err);
      });
    };

    vm.getData();

    vm.contactContractor = function () {
        var objContact = {};       
        objContact.contractorId = vm.contractorID;
        dataService.contactMeInfo(objContact).then(function (resp) {
            toastr.success("Message Sent to Contractor");
        }, function (err) {
            throw new Error("Failed Contact Contractor");
        })
    };
  }
})();
