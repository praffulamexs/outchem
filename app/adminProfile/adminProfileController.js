(function() {
    'use strict';

    angular
        .module('app')
        .controller('AdminProfileController', AdminProfileController);

    AdminProfileController.$inject = ['$scope', '$uibModal', '$window', '$state', 'dataService', '$timeout', 'toastr', 'bsLoadingOverlayService'];

    function AdminProfileController($scope, $uibModal, $window, $state, dataService, $timeout, toastr, bsLoadingOverlayService) {
        var vm = this;

        vm.categoryToEdit = {}
        vm.keywordToEdit = {};
        vm.packageToEdit = {};
        vm.pricingToEdit = {};
        vm.facilityToEdit = {};
        vm.affAssociationToEdit = {};

        vm.isPriceDirty = false;

        vm.getCategoryTemplate = function(category) {
            if (category.id === vm.categoryToEdit.id) return 'editCategory';
            else return 'displayCategory';
        };

        vm.getKeywordTemplate = function(keyword) {
            if (keyword.keywordId === vm.keywordToEdit.keywordId) return 'editKeyword';
            else return 'displayKeyword';
        };

        vm.getPackageTemplate = function(pkg) {
            if (pkg.pId === vm.packageToEdit.pId) return 'editPackage';
            else return 'displayPackage';
        };

        vm.getPricingTemplate = function(pricing) {
            if (pricing.from === vm.pricingToEdit.from) return 'editPricing';
            else return 'displayPricing';
        };

        vm.getFacilitiesTemplate = function (facility) {
            if (facility.facilityID === vm.facilityToEdit.facilityID) return 'editFacility';
            else return 'displayFacility';
        }

        vm.getAffAssociationTemplate = function (affAssociation) {
            if(affAssociation.affAssociationID === vm.affAssociationToEdit.affAssociationID) return 'editAffAssociation';
            else return 'displayAffAssociation';
        }

        vm.categoryHeaders = ["Name", "Description", "<", ">"];
        vm.keywordHeaders = ["Keyword", "Category", "<", ">"];
        vm.packageListingHeaders = ["Name", "Days", "<", ">"];
        vm.pricingHeaders = ["From", "To", "Price", ""];
        vm.keywordDetailsHeaders = ["Keyword Name", "Category Name", "Rank", "Price", "Count", "Contractors", "Buyers"];
        vm.facilitiesHeaders = ["Name", "<", ">"];
        vm.affAssociationHeaders = ["Name", "<", ">"];
        vm.remindersHeader = ["Buyer Company", "Contractor Company", "Action", "Last Reminder Date"];

        activate();

        function activate() {
            bsLoadingOverlayService.start();

            // call data services method to get data

            updateCateoryData();

            updateKeywordData();

            updatePackageData();

            updatePricingData();

            getKeywordDetailsData();

            updateFacilitiesData();

            updateAffAssociationData();

            updateRemindersData();

            // vm.pricingData = dummyPricingData.pricingModel;
            // vm.startingFromRank = Math.max.apply(Math, vm.pricingData.map(function(o) {
            //     return o.to;
            // })) + 1;
            // vm.basePrice = parseInt(dummyPricingData.basePrice);
            // console.log(vm.startingFromRank);

            bsLoadingOverlayService.stop();
        }

        function updateCateoryData() {
            dataService.getCategories().then(function(response) {

                vm.categoryData = response.data.categories;
            });
        }

        function updateKeywordData() {
            dataService.getAllKeywords().then(function(response) {
                vm.keywordData = response.data.keywords;

            });
        }

        function updatePackageData() {
            dataService.getPackage().then(function(response) {
                vm.packageListingData = response.data.packages;

            });
        }

        function updatePricingData() {
            dataService.getPricing().then(function(response) {
                vm.pricingData = response.data.pricingModel;
                vm.basePrice = parseInt(response.data.basePrice) || vm.pricingData[vm.pricingData.length - 1].basePrice || vm.pricingData[vm.pricingData.length - 1].price;
                vm.pricingData.splice(-1,1);
                vm.startingFromRank = Math.max.apply(Math, vm.pricingData.map(function(o) {
                    return o.to;
                })) + 1;
                vm.pricing = vm.pricing || {};
                vm.base = vm.base || {};
                vm.base.basePrice = vm.basePrice;
                vm.pricing.from = vm.startingFromRank;

            });
        }

        function getKeywordDetailsData() {
            dataService.getKeywordDetails().then(function(response) {
                vm.keywordDetailsData = response.data;
                var i =1;
                vm.keywordDetailsData.map(function (element) {
                    element.rank = i;
                    i++;
                });

            })
        }

        function updateFacilitiesData() {
            dataService.getFacilities().then(function(response) {
                if(response.status === 200){
                    vm.facilitiesData = response.data.facilities;
                }else{
                    vm.facilitiesData = [];
                }
            })
        }

        function updateAffAssociationData() {
            dataService.getAffiliations().then(function(response) {
                if(response.status === 200){
                    vm.affAssociationsData = response.data.affAsso;
                }else{
                    vm.affAssociationsData = [];
                }
            })
        }

        function updateRemindersData() {
            dataService.getRemindersList().then(function (response) {
                if (response.status === 200) {
                    vm.remindersData = response.data;
                }else{
                    vm.remindersData = [];
                }
            })
        }

        vm.openCategoryModal = function(categoryNameToAdd, categoryResults) {
            if (categoryResults.length === 1 && categoryNameToAdd.name === categoryResults[0].name) {
                toastr.warning("Cannot Create a new Category as it already Exists");
                return;
            }
            if (categoryNameToAdd && categoryNameToAdd.name) {
                vm.modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/adminProfile/CategoryAddModal.html',
                    scope: $scope
                });

            } else {
                toastr.warning("Category Name should not be blank");
            }
        }

        vm.cancelCategoryModal = function(categoryToCancel) {
            vm.modalInstance.dismiss();
            categoryToCancel.description = "";
        }

        vm.addCategory = function(categoryNameToAdd, categoryForm) {
            bsLoadingOverlayService.start();
            categoryNameToAdd.description = categoryNameToAdd.description || "No Description";
            var newCategory = {
                categoryName: categoryNameToAdd.name,
                categoryDescription: categoryNameToAdd.description
            };
            dataService
                .saveCategories(newCategory)
                .then(function(response) {

                    if (response.status === 201) {
                        updateCateoryData();
                        updateKeywordData();
                        getKeywordDetailsData();

                        vm.modalInstance.dismiss();
                        toastr.success("Added New Category");
                        vm.category = {};
                        console.log(categoryForm);
                        categoryForm.$setPristine();
                    } else {
                        vm.modalInstance.dismiss();
                        toastr.error("Something went wrong !! " + response.status);
                    }
                    bsLoadingOverlayService.stop();
                });
            bsLoadingOverlayService.stop();
        }

        vm.categoryEdit = function(row) {
            debugger;
            vm.categoryToEdit = angular.copy(row);

        }

        vm.categoryDelete = function(categoryToDelete) {
            var answer = $window.confirm("Are you sure you want to delete " + categoryToDelete.name);
            if (answer) {
                bsLoadingOverlayService.start();

                dataService.deleteCategories(categoryToDelete.id).then(function(response) {

                    if (response.status === 200) {
                        updateCateoryData();
                        updateKeywordData();
                        getKeywordDetailsData();
                        toastr.success("Deleted the Category");

                    } else {
                        toastr.error("Something went wrong !! " + response.status);
                    }
                    bsLoadingOverlayService.stop();
                });
            }

        }

        vm.categorySave = function(categoryToSave) {
            bsLoadingOverlayService.start();
            var saveCategory = {
                categoryId: categoryToSave.id,
                categoryName: categoryToSave.name,
                categoryDescription: categoryToSave.description,
                activeFlag: categoryToSave.activeFlag
            };
            console.log(saveCategory);
            dataService.updateCategories(saveCategory).then(function(response) {

                if (response.status === 200) {
                    updateCateoryData();
                    updateKeywordData();
                    getKeywordDetailsData();
                    vm.categoryToEdit = {};
                    toastr.success("Saved the Category");
                } else {
                    toastr.error("Something went wrong !! " + response.status);
                }
                bsLoadingOverlayService.stop();
            })
        }

        vm.categoryCancel = function(categoryToCancel) {
            vm.categoryToEdit = {};
        }

        vm.openKeywordModal = function(keywordNameToAdd, keywordResult, input1, input2) {
            if (input1) {
                toastr.error("Keyword Name cannot be blank");
                return;
            }
            if (input2) {
                toastr.error("Keyword Name's Category cannot be blank");
                return;
            }
            if (keywordResult.length === 1 && keywordResult[0].keywordName === keywordNameToAdd.name && keywordResult[0].categoryId === keywordNameToAdd.category) {
                toastr.warning("Cannot Create a new Keyword as the same name already exists.");
                return;
            }
            if (keywordNameToAdd && keywordNameToAdd.name && keywordNameToAdd.category) {
                vm.modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/adminProfile/KeywordAddModal.html',
                    scope: $scope
                });

            } else {
                toastr.warning("Keyword Name/ Category should not be blank");
            }
        }

        vm.cancelKeywordModal = function(keywordToCancel) {
            vm.modalInstance.dismiss();
            keywordToCancel.description = "";
        }

        vm.addKeyword = function(keywordToAdd, keywordForm) {
            bsLoadingOverlayService.start();
            var newKeyword = {
                keywordName: keywordToAdd.name,
                keywordDescription: keywordToAdd.description || "No Description",
                categoryId: keywordToAdd.category
            };

            dataService.saveKeyword(newKeyword).then(function(response) {

                if (response.status === 201) {
                    toastr.success("Added New Keyword");
                    updateKeywordData();
                    getKeywordDetailsData();
                    vm.modalInstance.dismiss();
                    keywordForm.$setPristine();
                    vm.keyword = {};
                } else {
                    vm.modalInstance.dismiss();
                    toastr.error("Something went wrong !! " + response.status);
                }
                bsLoadingOverlayService.stop();
            });
        }

        vm.keywordEdit = function(row) {
            vm.keywordToEdit = angular.copy(row);
        }

        vm.keywordSave = function(keywordToSave) {
            bsLoadingOverlayService.start();
            var dataToSave = {
                keywordId: keywordToSave.keywordId,
                categoryId: keywordToSave.categoryId,
                keywordName: keywordToSave.keywordName,
                keywordDescription: keywordToSave.description
            };
            dataService.updateKeyword(dataToSave).then(function(response) {

                if (response.status === 200) {
                    vm.keywordToEdit = {};
                    updateKeywordData();
                    getKeywordDetailsData();
                    toastr.success("Saved Keyword");
                } else {
                    toastr.error("Something went wrong " + response.status);
                }
                bsLoadingOverlayService.stop();
            });
        }

        vm.keywordCancel = function() {
            vm.keywordToEdit = {};
        }

        vm.keywordDelete = function(keywordToDelete) {
            var answer = $window.confirm("Are you sure you want to delete " + keywordToDelete.keywordName);
            console.log(keywordToDelete);
            if (answer) {
                bsLoadingOverlayService.start();
                dataService.deleteKeyword(keywordToDelete.keywordId).then(function(response) {

                    if (response.status === 200) {
                        updateKeywordData();
                        getKeywordDetailsData();
                        toastr.success("Deleted Keyword");
                    } else {
                        toastr.error("Something Went Wrong !! " + response.status);
                    }
                    bsLoadingOverlayService.stop();
                })
            }
        }

        vm.openListPackageModal = function(listPackage, packageResult, input1) {
            if (input1) {
                toastr.error("Package Name cannot be blank");
                return;
            }
            if (packageResult.length === 1 && packageResult[0].name === listPackage.name) {
                toastr.warning("Cannot Create a new package. A package with same name already exists.");
                return;
            }
            if (listPackage && listPackage.name) {
                vm.modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/adminProfile/PackageAddModal.html',
                    scope: $scope
                });

            } else {
                toastr.warning("Package Name should not be blank");
            }

        }

        vm.cancelListPackageModal = function(listPackage) {
            vm.modalInstance.dismiss();
            keywordToCancel.days = 0;
        }

        vm.addListPackage = function(listPackage, packageForm) {
            bsLoadingOverlayService.start();
            if (listPackage && listPackage.name && parseInt(listPackage.days) > 0) {
                var newPackage = {
                    packageName: listPackage.name,
                    packageDays: parseInt(listPackage.days)
                };

                dataService.savePackage(newPackage).then(function(response) {
                    if (response.status === 201) {
                        updatePackageData();
                        vm.package = {};
                        vm.modalInstance.dismiss();
                        toastr.success("Added new Package");
                        packageForm.$setPristine();
                    } else {
                        vm.modalInstance.dismiss();
                        toastr.error("Something went wrong !! " + response.status);
                    }
                    bsLoadingOverlayService.stop();
                })
            }
        }

        vm.editPackage = function(pkg) {
            vm.packageToEdit = angular.copy(pkg);
        }

        vm.savePackage = function(pkgToSave) {
            bsLoadingOverlayService.start();
            debugger;
            var dataToSave = {
                packageId: pkgToSave.pId,
                packageName: pkgToSave.name,
                packageDays: pkgToSave.days,
                activeFlag: pkgToSave.activeFlag
            };
            dataService.updatePackage(dataToSave).then(function(response) {
                if (response.status === 200) {
                    vm.packageToEdit = {};
                    updatePackageData();
                    toastr.success("Saved Package");
                } else {
                    toastr.error("Something went wrong " + response.status);
                }
                bsLoadingOverlayService.stop();
            });

        }

        vm.cancelPackage = function(pkg) {
            vm.packageToEdit = {};
        }

        vm.deletePackage = function(pkgToDelete) {
            var answer = $window.confirm("Are you sure you want to delete " + pkgToDelete.keywordName);
            if (answer) {
                bsLoadingOverlayService.start();
                dataService.deletePackage(pkgToDelete.id).then(function(response) {

                    if (response.status === 200) {
                        updatePackageData()();
                        toastr.success("Deleted Package");
                    } else {
                        toastr.error("Something Went Wrong !! " + response.status);
                    }
                    bsLoadingOverlayService.stop();
                })
            }
        }

        vm.openPriceModal = function() {
            vm.modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/adminProfile/PricingSaveModal.html',
                scope: $scope
            });
        }

        vm.cancelPriceModal = function() {
            vm.modalInstance.dismiss();
        }

        vm.addPrice = function(priceToAdd) {

            if(priceToAdd.to < priceToAdd.from){
                toastr.error("Please check the Range", "Incorrect Range");
                return;
            }
            if (!vm.pricingData.length) {
                if (vm.pricing.from === vm.startingFromRank) {
                    if (vm.pricing.price > vm.basePrice) {
                        var newPrice = {
                            from: parseInt(vm.pricing.from),
                            to: parseInt(priceToAdd.to),
                            price: parseInt(priceToAdd.price),
                            basePrice: vm.basePrice
                        };

                        vm.pricingData.push(newPrice);

                        toastr.success("Saved Locally. Please click on update price in order to sync the changes with server.");
                        vm.pricing = {};

                        vm.pricing.from = parseInt(priceToAdd.to) + 1;
                        vm.startingFromRank = vm.pricing.from;
                        vm.isPriceDirty = true;
                    } else {
                        toastr.error("Price should be greater than Base Price " + vm.base.basePrice);
                    }
                } else {
                    toastr.error("Cannot Save Data. Please verify the From Days.");
                }

            } else {
                if (vm.pricing.from === vm.startingFromRank) {
                    if (!parseInt(vm.pricing.from) && !parseInt(priceToAdd.to) && !parseInt(priceToAdd.price)) {
                        toastr.error("Incorrect Data. Please check your data before inserting in the table.");
                        return;
                    }
                    if (priceToAdd.price > vm.pricingData[vm.pricingData.length - 1].price) {
                        toastr.error("Price cannot be greater than " + vm.pricingData[vm.pricingData.length - 1].price);
                        return;
                    }
                    if (priceToAdd.price < vm.basePrice) {
                        toastr.error("Price should be greater than Base Price : " + vm.basePrice);
                        return;
                    }
                    var newPrice = {
                        from: parseInt(vm.pricing.from),
                        to: parseInt(priceToAdd.to),
                        price: parseInt(priceToAdd.price),
                        basePrice: 100
                    };

                    vm.pricingData.push(newPrice);
                    toastr.success("Saved Locally. Please click on update price in order to sync the changes with server.");
                    vm.pricing = {};

                    vm.pricing.from = parseInt(priceToAdd.to) + 1;
                    vm.startingFromRank = vm.pricing.from;
                    vm.isPriceDirty = true;
                } else {
                    toastr.error("Cannot Save Data. Please verify the From Days.");
                }
            }
        }

        vm.editPrice = function(priceToEdit) {
            vm.pricingToEdit = angular.copy(priceToEdit);

        }

        vm.savePrice = function(priceToSave, index) {
            if (priceToSave.price < 0) {
                toastr.error("Price cannot be empty or zero");
                return;
            }
            if (vm.pricingData.length < (index + 1)) {

                if (priceToSave.price < vm.basePrice) {
                    toastr.error("Price updated for range " + priceToSave.from + "-" + priceToSave.to + " should be greater than Base Price : " + vm.basePrice);
                    return;
                }

                var upperPriceLimit = priceToSave.price > vm.pricingData[index + 1].price;

                if (priceToSave.price < vm.pricingData[index - 1].price && upperPriceLimit) {
                    vm.pricingData[index] = angular.copy(priceToSave);
                    var tempData = vm.pricingData;

                    toastr.success("Saved locally. Please click on update price in order to sync the changes with server.");
                    vm.isPriceDirty = true;
                    vm.pricingToEdit = {};
                } else {
                    toastr.error("Updated Price is invalid. Please give it a check.");
                }
            } else {
                if (priceToSave.price < vm.basePrice) {
                    toastr.error("Price updated for range " + priceToSave.from + "-" + priceToSave.to + " should be greater than Base Price : " + vm.basePrice);
                    return;
                }
                if (index > 0 && priceToSave.price < vm.pricingData[index - 1].price) {
                    vm.pricingData[index] = angular.copy(priceToSave);
                    toastr.success("Saved locally. Please click on update price in order to sync the changes with server.");
                    vm.isPriceDirty = true;
                    vm.pricingToEdit = {};
                } else if (index === 0) {
                    if (priceToSave.price < vm.basePrice) {
                        toastr.error("Price updated for range " + priceToSave.from + "-" + priceToSave.to + " should be greater than Base Price : " + vm.basePrice);
                        return;
                    }

                    vm.pricingData[index] = angular.copy(priceToSave);

                    toastr.success("Saved locally. Please click on update price in order to sync the changes with server.");
                    vm.isPriceDirty = true;
                    vm.pricingToEdit = {};

                } else {
                    toastr.error("Updated Price is invalid. Please give it a check.");
                }
            }
        }

        vm.cancelPrice = function() {
            vm.pricingToEdit = {};
        }

        vm.deletePrice = function() {
            var answer = $window.confirm("Are you sure you want to Delete all Pricing and start again ?");
            if (answer) {
                vm.pricingData = [];
                vm.pricing.from = 1;
                vm.startingFromRank = 1;
                vm.isPriceDirty = false;
            }
        }

        vm.updatePrice = function(basePrice) {
            console.log(vm.pricingData);
            console.log(vm.startingFromRank);
            if (basePrice === null || basePrice < 1){
                toastr.error("Base Price cannot be empty of zero");
                return;
            }
            if (basePrice > vm.pricingData[vm.pricingData.length -1].price) {
                toastr.error("Base Price is Invalid");
                return;
            }
            var answer = $window.confirm("Are you sure you want to update the pricing ?");
            if (answer) {
                if (parseInt(basePrice) > vm.pricingData[vm.pricingData.length - 1].price) {
                    toastr.error("Base Price cannot be greater than " + vm.pricingData[vm.pricingData.length - 1].price);
                    return;
                }
                bsLoadingOverlayService.start();

                var lastPricing = {
                    from: vm.startingFromRank,
                    to : 0,
                    price : basePrice
                };
                vm.pricingData.push(lastPricing);

                var dataToSave = {
                    lstPricingModel : vm.pricingData
                };

                dataService.updatePricing(dataToSave).then(function(response) {
                    if (response.status === 200) {
                        updatePricingData();
                        toastr.success("Updated Pricing Models");
                        vm.isPriceDirty = false;
                    } else {
                        toastr.error("Something went wrong !! " + response.status);
                    }
                    vm.modalInstance.dismiss();
                    bsLoadingOverlayService.stop();
                })
            }
        }

        vm.openContractorListModal = function (data) {
            if (data.contractorCount > 0) {
                dataService.findContractor(data.keywordId).then(function(response) {
                    if (response.status === 200) {
                        vm.contractorList = response.data;
                        vm.modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: 'app/adminProfile/ContractorListModal.html',
                            scope: $scope
                        });

                    }
                })
            }
        }

        vm.openBuyerListModal = function (data) {
            if(data.buyerCount > 0){
                dataService.findBuyer(data.keywordId).then(function (response) {
                    if (response.status === 200) {
                        vm.buyerList = response.data;
                        vm.modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: 'app/adminProfile/BuyerListModal.html',
                            scope: $scope
                        });
                    }
                })
            }
        }

        vm.addFacility = function (result, input1) {
            if (input1) {
                toastr.error("Invalid Input","Please Check inputs.");
                return;
            }
            if(result.length === 1 && result[0].facilityName === vm.facility.name){
                toastr.error("Facility Already Exists.");
                return;
            }
            var dataToSave = {
                facilityName : vm.facility.name
            };
            dataService.saveFacility(dataToSave).then(function (response) {
                if (response.status === 200) {
                    toastr.success("Added new Facility.");
                    updateFacilitiesData();
                    vm.facility = {};
                }else{
                    toastr.error("Something went wrong ! " + response.status);
                }
            });
        }

        vm.editFacilty = function (facilityToEdit) {
            vm.facilityToEdit = angular.copy(facilityToEdit);
        }

        vm.deleteFacility = function (facilityToDelete) {
            var answer = $window.confirm("Are you sure you want to delete " + facilityToDelete.facilityName);
            if (answer) {
                dataService.deleteFacility(facilityToDelete.facilityID).then(function (response) {
                    if (response.status === 200) {
                        vm.facility = {};
                        updateFacilitiesData();
                        toastr.success("Deleted Facility Successfully.");
                    } else{
                        toastr.error("Something went wrong ! " + response.status);
                    }
                });
            }            
        }

        vm.saveFacility = function (facilityToSave) {
            if (facilityToSave.facilityName.length > 0) {
                dataService.editFacility(facilityToSave).then(function (response) {
                    if(response.status === 200){
                        toastr.success("Successfully updated Facility.");
                        vm.facilityToEdit = {};
                        vm.facility = {}; 
                        updateFacilitiesData();
                    } else{
                        toastr.error("Something went wrong ! " + response.status);
                    }
                })
            }else{
                toastr.error("Facility Name cannot be blank.");
            }
        }

        vm.cancelFacility = function (facilityToCancel) {
            vm.facilityToEdit = {};
        }

        vm.addAffAssociation = function (result, input1) {
            if (input1) {
                toastr.error("Invalid Input","Please Check inputs.");
                return;
            }
            if(result.length === 1 && result[0].affAssociationName === vm.affAssociation.name){
                toastr.error("Affiliation already exists.");
                return;
            }
            var dataToSave = {
                affAssociationName : vm.affAssociation.name
            };
            dataService.saveAffiliation(dataToSave).then(function (response) {
                if (response.status === 200) {
                    toastr.success("Added new Affiliation.");
                    updateAffAssociationData();
                    vm.affAssociation = {};
                }else{
                    toastr.error("Something went wrong ! " + response.status);
                }
            });
        }

        vm.editAffAssociation = function (affAssociationToEdit) {
            vm.affAssociationToEdit = angular.copy(affAssociationToEdit);
        }

        vm.deleteAffAssociation = function (affAssociationToDelete) {
            var answer = $window.confirm("Are you sure you want to delete " + affAssociationToDelete.affAssociationName);
            if (answer) {
                dataService.deleteAffiliation(affAssociationToDelete.affAssociationID).then(function (response) {
                    if (response.status === 200) {
                        vm.affAssociation = {};
                        updateAffAssociationData();
                        toastr.success("Deleted Affiliation Successfully.");
                    } else{
                        toastr.error("Something went wrong ! " + response.status);
                    }
                });
            }
        }

        vm.saveAffAssociation = function (affAssociationToSave) {
            if (affAssociationToSave.affAssociationName.length > 0) {
                dataService.editAffiliation(affAssociationToSave).then(function (response) {
                    if(response.status === 200){
                        toastr.success("Successfully updated Affiliation.");
                        vm.affAssociationToEdit = {};
                        vm.affAssociation = {}; 
                        updateAffAssociationData();
                    }else{
                        toastr.error("Something went wrong ! " + response.status);
                    }
                })
            }else{
                toastr.error("Affiliation name cannot be blank.");
            }
        }

        vm.cancelAffAssociation = function (affAssociationToCancel) {
            vm.affAssociationToEdit = {};
        }

        vm.sendReminder = function (reminder) {
            var answer = $window.confirm("Are you sure you want to send a reminder? ");
            if (answer) {
                var dataToSend = {
                    buyerId: reminder.buyerId,
                    contractorId: reminder.contractorId
                };
                dataService.reviewReminder(dataToSend).then(function (respnonse) {
                    if (response.status === 200) {
                        updateRemindersData();
                        toastr.success("Successfully Sent Review Reminder to " + reminder.buyerCompanyName);
                    }else{
                        toastr.error("Something went wrong! " + response.status);
                    }
                })
            }
        }

    }
})();
