(function () {
  'use strict';

  angular
    .module('app')
    .controller('BuyerProfileController', BuyerProfileController);

  BuyerProfileController.$inject = ['$scope', '$filter', '$state', 'dataService', '$timeout', 'toastr', 'bsLoadingOverlayService', 'UserService', 'Utils'];

  function BuyerProfileController($scope, $filter, $state, dataService, $timeout, toastr, bsLoadingOverlayService, UserService, Utils) {
    var vm = this;
    vm.buyer = {};
    vm.selectedKeywords = [];
    vm.saveModel = {};
    vm.countries = [
      {
        countryID: 1, name: "India"
      }
    ];

    function bindData(_data) {
      vm.buyer.company = _data.companyName;
      vm.buyer.address = _data.addressOfBusiness;
      if (_data.addedKeywords) {
        vm.selectedKeywords = _data.addedKeywords;
      }
    }

    vm.getData = function () {
      bsLoadingOverlayService.start();
      dataService.getBuyerInformation().then(function (resp) {
        bindData(resp.data);
        bsLoadingOverlayService.stop();
      }, function (err) {
        bsLoadingOverlayService.stop();
        console.log('Error In getContractor', err);
      });
    };

    activate();

    function activate() {
      bsLoadingOverlayService.start();
      vm.user = UserService.getCurrentUser();
      dataService.getLoggedInUser(vm.user.accessToken).then(function (resp) {
        if (resp.status === 200) {
          Utils.isImage(dataService.getImageUrl() + resp.data + '.jpg').then(function (result) {
            if (result) {
              $scope.tempUrl = dataService.getImageUrl() + resp.data + '.jpg';
            }
          });
        }
        bsLoadingOverlayService.stop();
      }, function () {
        bsLoadingOverlayService.stop();
      });
      vm.user.countryName = $filter('filter')(vm.countries, {countryID: parseInt(vm.user.country, 16)});
      if (vm.user.countryName) {
        vm.user.countryName = vm.user.countryName[0].name;
      }
      vm.getData();
      bsLoadingOverlayService.stop();
    }
	
	vm.editProfile = function(field) {
		if(field === 'phone')
		{
			vm.edtPhone = false;
			bsLoadingOverlayService.start();
			dataService.editUser(vm.user).then(function (resp) {
				if (resp.status === 200) {
					toastr.success("Phone Updated Successful");
					vm.user.phoneNumberConfirmed = false;
				}
				bsLoadingOverlayService.stop();
			  }, function () {
			    toastr.error("Error");
				bsLoadingOverlayService.stop();
			 });
		} else {
			vm.edtName = false;
			bsLoadingOverlayService.start();
			dataService.editUser(vm.user).then(function (resp) {
				if (resp.status === 200) {
					toastr.success("Name Updated Successful");					
				}
				bsLoadingOverlayService.stop();
			  }, function () {
			    toastr.error("Error");
				bsLoadingOverlayService.stop();
			 });
		}		
	};

    vm.validateForm = function () {
      if (angular.isUndefined(vm.buyer.company) || vm.buyer.company === null || vm.buyer.company === '') {
        vm.companyError = true;
      } else {
        vm.companyError = false;
      }
      if (angular.isUndefined(vm.buyer.address) || vm.buyer.address === null || vm.buyer.address === '') {
        vm.addressError = true;
      } else {
        vm.addressError = false;
      }

      if (!vm.companyError && !vm.addressError) {
        vm.saveModel.companyName = vm.buyer.company;
        vm.saveModel.addressOfBusiness = vm.buyer.address;
        vm.saveModel.profilePicture = '';
        vm.saveModel.keywords = vm.selectedKeywords;

        bsLoadingOverlayService.start();
        dataService.saveBuyerInformation(vm.saveModel).then(function (result) {
          bsLoadingOverlayService.stop();
          vm.buyerSuccess = true;
          $timeout(function () {
            vm.buyerSuccess = false;
          }, 5000);
          vm.enbCompanyName = false;
          vm.enbCompanyAdd = false;
          console.log(result);
        }, function (err) {
          bsLoadingOverlayService.stop();
          console.log(err);
        });
      }
    };

    vm.getKeywords = function (key) {
      return dataService.getKeywordBySearchKey(key).then(function (resp) {
        return resp.data.keywords.map(function (item) {
          return item;
        });
      }, function (err) {
        console.log('Error', err);
        return [];
      });
    };

    vm.addKeyword = function (item) {
      vm.selectedKeywords.push(item);
      vm.buyerDetail.Keyword = '';
    };

    vm.removeKeyword = function (indx) {
      vm.selectedKeywords.splice(indx, 1);
    };

    vm.verifyMobile = function () {
      vm.showOTP = true;
      dataService.verifyPhone('091' + vm.user.phone, vm.user.accessToken).then(function (resp) {
        if (resp.status === 200) {
          vm.verifyMobileClk = true;
          toastr.success("OTP Sent Successful");
        } else {
          toastr.error("Error In Sending OTP");
        }
      }, function (err) {
        toastr.error("Error In Sending OTP");
        console.log('validate OTP error', err);
      });
    };

    vm.validOTP = function () {
      vm.showOTP = false;
      dataService.verifyPhoneCode('091' + vm.user.phone, vm.OTP, vm.user.accessToken).then(function (resp) {
        if (resp.status === 200) {
          vm.user.phoneNumberConfirmed = true;
          toastr.success("OTP Validated Successful");
        } else {
          toastr.error("OTP Validation error");
        }
      }, function (err) {
        toastr.error("OTP Validation error");
        console.log('OTP Validation error', err);
      });
    };

    vm.resendOTP = function () {
      console.log('resend');
    };

    $scope.$watch('model.profPic', function (newFile) {
      if (angular.isUndefined(newFile) === false) {
        $scope.tempUrl = URL.createObjectURL(newFile);
        dataService.imageUpload($scope.model.profPic).then(function (resp) {
          console.log(resp);
          if (resp.status === 201) {
            toastr.success("Profile Pic Saved Successful");
          } else {
            toastr.error("Profile Pic Saving Failed");
          }
        }, function (err) {
          console.log('Image Upload error', err);
          toastr.error("Profile Pic Saving Failed");
        });
      } else {
        $scope.tempUrl = '';
        // service call
      }
    });
  }
})();
