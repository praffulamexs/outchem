(function() {
    'use strict';

    angular
        .module('app')
        .controller('HeaderController', HeaderController);

    HeaderController.$inject = ['$uibModal', 'UserService', '$rootScope', '$state', 'dataService', 'Utils', '$location', '$anchorScroll', '$document'];

    function HeaderController($uibModal, UserService, $rootScope, $state, dataService, Utils, $location, $anchorScroll, $document) {
        var vm = this;
        vm.data = {};
        activate();
		
        function activate() {
            var user = UserService.getCurrentUser();
            if (user) {
                vm.data.user = user;
                vm.data.isLoggedIn = true;

                dataService.getLoggedInUser(vm.data.user.accessToken).then(function(resp) {
                    if (resp.status === 200) {
                        Utils.isImage(dataService.getImageUrl() + resp.data + '.jpg').then(function(result) {
                            if (result) {
                                vm.tempUrl = dataService.getImageUrl() + resp.data + '.jpg';
                            }
                        });
                    }
                });
            }
        }
		
		vm.trmCondPopUp = function(){
			vm.modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'app/misc/terms.html'          
            });
		};
		
		vm.privacyPolPopUp = function(){
			vm.modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'app/misc/privacy.html'          
            });
		}; 
		
        vm.openMenu = function($mdMenu, ev) {
            $mdMenu.open(ev);
        };

        vm.clkAbtUs = function() {
            $document.scrollTo(document.getElementById("aboutuscontent"), "", 2000);
            //$location.hash('aboutuscontent');
            //$anchorScroll();
        };

        $rootScope.$on('authorized', function() {
            var user = UserService.getCurrentUser();
            if (user) {
                vm.data.user = user;
                vm.data.isLoggedIn = true;
            }
        });

        $rootScope.$on('signOut', function() {
            vm.data.user = null;
            vm.data.isLoggedIn = false;
        });

        vm.login = function() {
            $uibModal.open({
                animation: true,
                templateUrl: '/app/login/loginModal.html',
                controller: "LoginController",
                controllerAs: "loginController"
            });
        };

        vm.signUp = function() {
            $uibModal.open({
                animation: true,
                templateUrl: '/app/signUp/signUpModal.html'
            });
        };

        vm.viewProfile = function() {
            if (vm.data.user.isAdmin) {
                $state.go('adminProfile');
            } else if (vm.data.user.isContractor) {
                $state.go("contractorProfile");
            } else {
                $state.go("buyerProfile");
            }
        };

        vm.viewDashboard = function() {
            if (vm.data.user.isAdmin) {
                $state.go('adminDashboard');
            } else if (vm.data.user.isContractor) {
                $state.go('companyDashboard');
            }
        }

        vm.goHome = function() {
            $state.go("app");
        };

        vm.viewCompanyDashboard = function() {
            $state.go("companyDashboard");
        }

        vm.viewAdminProfile = function() {
            $state.go("adminProfile");
        }

        vm.signOut = function() {
            UserService.signOutUser();
            $rootScope.$broadcast('signOut');
            $state.go("app");
        };

        vm.showVideo = function() {
            $uibModal.open({
                animation: true,
                size: 'lg',
                templateUrl: '/app/misc/playVideo.html'
            });
        };
    }
})();
