(function () {
  'use strict';
  angular
    .module('app')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['$rootScope', '$scope', 'UserService', '$state', 'toastr', '$timeout', 'dataService', 'bsLoadingOverlayService', '$uibModalInstance'];

  function LoginController($rootScope, $scope, UserService, $state, toastr, $timeout, dataService, bsLoadingOverlayService, $uibModalInstance) {
    var vm = this;
    vm.data = {};

    vm.doLogin = function () {
      bsLoadingOverlayService.start();
      vm.data.userId = vm.data.userId.trim();
      dataService.doLogin(vm.data).then(function (data) {
        if (data) {
          dataService.getUserInfo(data.access_token).then(function (response) {
            UserService.setCurrentUser(response, data.access_token);
            $rootScope.$broadcast('authorized');
            $uibModalInstance.close(response);
            if (response.isAdmin) {
              $state.go('adminProfile');
            }else if (response.isContractor) {
              $state.go('contractorProfile');
            } else {
              $state.go('buyerProfile');
            }
          });
        } else {
          $rootScope.$broadcast('unauthorized');
          vm.invalidCredentials = true;
        }
        bsLoadingOverlayService.stop();
      });
    };

    vm.validateForm = function () {
      console.log("awesome!!");
      if (angular.isUndefined(vm.data.userId) || vm.data.userId === null || vm.data.userId === '') {
        vm.userIdError = true;
      } else {
        vm.userIdError = false;
      }
      if (angular.isUndefined(vm.data.userPassword) || vm.data.userPassword === null || vm.data.userPassword === '') {
        vm.userPasswordError = true;
      } else {
        vm.userPasswordError = false;
      }
      if (!vm.userIdError && !vm.userPasswordError) {
        vm.doLogin();
      }
    };

    vm.forgetPassword = function () {
      if (angular.isUndefined(vm.data.userId) || vm.data.userId === null || vm.data.userId === '') {
        vm.userIdError = true;
      } else {
        vm.userIdError = false;
        bsLoadingOverlayService.start();
        dataService.forgetPassword(vm.data.userId).then(function (response) {
          if (response.status === 200) {
            toastr.success("Password Reset Link Sent to Your Email ID");
          }
          bsLoadingOverlayService.stop();
        });
      }
    };
  }
})();
