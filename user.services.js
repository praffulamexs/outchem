(function () {
  'use strict';

  angular
    .module('app')
    .service('UserService', UserService);

  UserService.$inject = ['store'];

  function UserService(store) {
    var vm = this;
    var currentUser = "";
    vm.getCurrentUser = getCurrentUser;
    vm.setCurrentUser = setCurrentUser;
    vm.signOutUser = signOutUser;

    function getCurrentUser() {
      if (!currentUser) {
        currentUser = store.get('user');
      }
      // console.log(currentUser);
      return currentUser;
    }

    function setCurrentUser(user, accessToken) {
      currentUser = user;
      currentUser.accessToken = accessToken;
      store.set('user', user);
      return currentUser;
    }

    function signOutUser() {
      currentUser = null;
      store.remove('user');
    }
  }
})();
